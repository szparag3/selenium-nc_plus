#!/usr/bin/env python3
from pyvirtualdisplay import Display
from selenium import webdriver
import time
import secret


display = Display(visible=1, size=(800, 600))
display.start()

browser = webdriver.Chrome()
browser.implicitly_wait(10)

#login to portal 
browser.get('https://moje.ncplus.pl/login.html#/')
browser.find_element_by_id('edit-name').send_keys(secret.ncLogin)
browser.find_element_by_id('edit-pass').send_keys(secret.ncPass)
browser.find_element_by_id('edit-pass').submit()
for i in range(5):
    time.sleep(1)
browser.get('https://moje.ncplus.pl/index.html#/finance/findetails')
browser.find_element_by_css_selector('button[type="submit"]').submit()
browser.find_element_by_name('payment_cards').send_keys(secret.cardType)
browser.find_element_by_name('payment_cards').submit()
for i in range(4):
    j = str(i+1)
    browser.find_element_by_name('PAN'+j).send_keys(secret.cardNo[i*4:(i*4)+4])

browser.find_element_by_name('Ecom_Payment_Card_ExpDate_Month').send_keys(secret.validMonth)
browser.find_element_by_name('Ecom_Payment_Card_ExpDate_Year').send_keys(secret.validYear)
browser.find_element_by_name('Ecom_Payment_Card_Verification').send_keys(secret.ccv)
browser.find_element_by_name('Ecom_Payment_Card_Verification').submit()

try:
    #visa verification
    PwdLabel = browser.find_element_by_css_selector('div#PWDSection span').text
    PwdLabel = PwdLabel.split(' ', 4)
    PwdLabel[0] = PwdLabel[0][:-1]
    numbers = ['first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth', 'eleventh', 'twelfth']

    NetPass = {}
    for i in range(len(secret.NetPass)):
        NetPass[numbers[i]] = secret.NetPass[i]

    pin1 = NetPass[PwdLabel[0]]
    pin2 = NetPass[PwdLabel[1]]
    pin3 = NetPass[PwdLabel[3]]


    browser.find_element_by_name('slotpin1').send_keys('pin1')
    browser.find_element_by_name('slotpin2').send_keys('pin2')
    browser.find_element_by_name('slotpin3').send_keys('pin3')
    browser.find_element_by_css_selector('input[type="submit"]').submit()
except:
    print('no visa verification')

browser.find_element_by_link_text("Przejdź do strony głównej").click()

##browser.quit()
##display.stop()